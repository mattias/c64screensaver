﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Reflection;

namespace Laserbrain.C64Screensaver
{
    internal class C64Font
    {
        private readonly C64Screen screen;
        private readonly C64Character[] characters = new C64Character[256];
        private readonly Dictionary<char, byte> charIndexDict = new Dictionary<char, byte>();

        public C64Font(string resourceName, C64Screen screen)
        {
            this.screen = screen;
            Assembly assembly = Assembly.GetExecutingAssembly();
            using (Stream imageStream = assembly.GetManifestResourceStream(resourceName))
            {
                if (imageStream != null)
                {
                    using (Bitmap fontBitmap = new Bitmap(imageStream))
                    {
                        if ((fontBitmap.Width != 256) && (fontBitmap.Height != 64))
                            throw new ArgumentException("Bad font image size, should be 256x64 pixels (32x8 characters)");
                        for (int i = 0; i < 256; i++)
                            characters[i] = new C64Character(GetCharacterMap(fontBitmap, i));
                    }
                }
            }
            InitCharIndex();
        }

        private static bool[,] GetCharacterMap(Bitmap fontBitmap, int charIndex)
        {
            bool[,] map = new bool[C64Character.Width, C64Character.Height];
            int charLeft = (charIndex % 32) * C64Character.Width;
            int charTop = (charIndex / 32) * C64Character.Height;
            for (int y = 0; y < C64Character.Height; y++)
                for (int x = 0; x < C64Character.Width; x++)
                    map[x, y] = (fontBitmap.GetPixel(charLeft + x, charTop + y).R < 200);
            return map;
        }

        private void InitCharIndex()
        {
            InitCharIndexLine(0, "@ABCDEFGHIJKLMNOPQRSTUVWXYZ[£]  ");
            InitCharIndexLine(1, " !\"#$%&'()*+,-./0123456789:;<=>?");
            InitCharIndexLine(4, "@abcdefghijklmnopqrstuvwxyz[£]  ");
            charIndexDict[' '] = 32;
        }

        private void InitCharIndexLine(int lineIndex, string characterLine)
        {
            byte index = (byte)(lineIndex * 32);
            foreach (char c in characterLine)
            {
                if (c != ' ')
                    charIndexDict[c] = index;
                index++;
            }
        }

        public void DrawChar(int x, int y, C64Palette? foreColor, C64Palette? backColor, byte charIndex)
        {
            characters[charIndex].Draw(x, y, foreColor, backColor, screen);
        }

        public void DrawChar(int x, int y, C64Palette? foreColor, C64Palette? backColor, char c)
        {
            byte charIndex;
            if (charIndexDict.TryGetValue(c, out charIndex))
                characters[charIndex].Draw(x, y, foreColor, backColor, screen);
        }

        public void DrawString(int x, int y, C64Palette? foreColor, C64Palette? backColor, string text)
        {
            foreach (char c in text)
                DrawChar(x++, y, foreColor, backColor, c);
        }

        public C64Character GetChar(char c)
        {
            byte charIndex;
            return charIndexDict.TryGetValue(c, out charIndex) ? characters[charIndex] : null;
        }
    }
}