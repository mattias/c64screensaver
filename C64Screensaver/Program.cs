﻿using System;
using System.Windows.Forms;

namespace Laserbrain.C64Screensaver
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        private static void Main(string[] args)
        {
            if (args.Length > 0)
            {
                switch (args[0].ToLower().Trim().Substring(0, 2))
                {
                    case "/p":
                        if (args.Length > 1)
                        {
                            Application.EnableVisualStyles();
                            Application.SetCompatibleTextRenderingDefault(false);
                            IntPtr intPtr = IntPtr.Zero;
                            try
                            {
                                intPtr = new IntPtr(long.Parse(args[1]));
                            }
                            catch { }
                            if (intPtr != IntPtr.Zero)
                                Application.Run(new MainForm(intPtr));
                        }
                        break;
                    case "/c":
                        break;
                    default:
                        RunScreensaver();
                        break;
                }
            }
            else
                RunScreensaver();
        }

        private static void RunScreensaver()
        {
            //run the screen saver
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            foreach (Screen screen in Screen.AllScreens)
            {
                //creates a form just for that screen
                //and passes it the bounds of that screen
                MainForm screensaver = new MainForm(screen.Bounds.Size);
                screensaver.Show();
            }
            Application.Run();
        }
    }
}