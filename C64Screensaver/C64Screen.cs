﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;

namespace Laserbrain.C64Screensaver
{
    internal class C64Screen : IDisposable
    {
        private static readonly Random Random = new Random();

        public const int ScreenWidth = 320;
        public const int ScreenHeight = 200;

        public readonly Graphics FullGraphics;

        private readonly int destinationWidth;
        private readonly int destinationHeight;

        private readonly bool smooth;

        internal readonly Bitmap OriginalScreen;
        internal readonly Bitmap FullScreen;
        internal readonly int ScreenLeft;
        internal readonly int ScreenTop;

        private readonly Rectangle backRect;
        private readonly Rectangle frameTopRect;
        private readonly Rectangle frameLeftRect;
        private readonly Rectangle frameBottomRect;
        private readonly Rectangle frameRightRect;

        public C64Screen(int destinationWidth, int destinationHeight, bool smooth, Size screenSize)
        {
            this.destinationWidth = destinationWidth;
            this.destinationHeight = destinationHeight;
            this.smooth = smooth;

            if (!smooth)
            {
                const float screenFrameRatio = 384f / 320f;
                int destScreenWidth = (int)(destinationWidth / screenFrameRatio) / 40 * 40;
                int destCharWidth = destScreenWidth / 40 / 8 * 8;
                int pixelWidth = destCharWidth / 8;
                int pixelHeight = destCharWidth / 8;

                FullScreen = new Bitmap(destinationWidth / pixelWidth, destinationHeight / pixelHeight);
            }
            else
            {
                const float screenFrameRatio = 384f / 320f;
                float destScreenWidth = (int)(destinationWidth / screenFrameRatio);
                float destCharWidth = destScreenWidth / 40;
                float pixelWidth = destCharWidth / 8f;
                float pixelHeight = destCharWidth / 8f;

                FullScreen = new Bitmap((int)(destinationWidth / pixelWidth), (int)(destinationHeight / pixelHeight));
            }

            ScreenLeft = (FullScreen.Width - ScreenWidth) / 2;
            ScreenTop = (FullScreen.Height - ScreenHeight) / 2;

            backRect = new Rectangle(ScreenLeft, ScreenTop, ScreenWidth, ScreenHeight);
            frameTopRect = new Rectangle(0, 0, FullScreen.Width, ScreenTop);
            frameLeftRect = new Rectangle(0, ScreenTop, ScreenLeft, ScreenHeight);
            frameBottomRect = new Rectangle(0, ScreenTop + ScreenHeight, FullScreen.Width, FullScreen.Height - ScreenTop - ScreenHeight);
            frameRightRect = new Rectangle(ScreenLeft + ScreenWidth, ScreenTop, FullScreen.Width - ScreenLeft - ScreenWidth, ScreenHeight);

            FullGraphics = Graphics.FromImage(FullScreen);

            using (Bitmap originalBitmap = new Bitmap(screenSize.Width, screenSize.Height))
            {
                using (Graphics g = Graphics.FromImage(originalBitmap))
                {
                    g.CopyFromScreen(Point.Empty, Point.Empty, screenSize);
                }

                OriginalScreen = new Bitmap(FullScreen.Width, FullScreen.Height);
                using (Graphics g = Graphics.FromImage(OriginalScreen))
                {
                    g.DrawImage(originalBitmap, 0, 0, OriginalScreen.Width, OriginalScreen.Height);
                }
            }
        }

        public void Dispose()
        {
            FullScreen.Dispose();
            OriginalScreen.Dispose();
        }

        public void Draw(Graphics g)
        {
            g.InterpolationMode = smooth ? InterpolationMode.High : InterpolationMode.NearestNeighbor;
            g.DrawImage(FullScreen, 0, 0, destinationWidth, destinationHeight);
        }

        public void Clear(C64Palette? backColor)
        {
            if (backColor.HasValue)
                FullGraphics.FillRectangle(C64Brushes.Items[(int)backColor], backRect);
            else
                FullGraphics.DrawImage(OriginalScreen, backRect, backRect, GraphicsUnit.Pixel);
        }

        public void DrawFrame(C64Palette? frameColor)
        {
            if (frameColor.HasValue)
            {
                Brush frameBrush = C64Brushes.Items[(int)frameColor];
                FullGraphics.FillRectangle(frameBrush, frameTopRect);
                FullGraphics.FillRectangle(frameBrush, frameLeftRect);
                FullGraphics.FillRectangle(frameBrush, frameRightRect);
                FullGraphics.FillRectangle(frameBrush, frameBottomRect);
            }
            else
            {
                FullGraphics.DrawImage(OriginalScreen, frameTopRect, frameTopRect, GraphicsUnit.Pixel);
                FullGraphics.DrawImage(OriginalScreen, frameLeftRect, frameLeftRect, GraphicsUnit.Pixel);
                FullGraphics.DrawImage(OriginalScreen, frameRightRect, frameRightRect, GraphicsUnit.Pixel);
                FullGraphics.DrawImage(OriginalScreen, frameBottomRect, frameBottomRect, GraphicsUnit.Pixel);
            }
        }

        public void DrawRaster(bool back, bool frame, int rasterWidth)
        {
            if (!back && !frame)
                return;

            FullGraphics.ResetClip();
            if (back && !frame)
                FullGraphics.SetClip(backRect, CombineMode.Replace);
            else if (!back)
                FullGraphics.SetClip(backRect, CombineMode.Exclude);

            int y = 0;
            do
            {
                FullGraphics.FillRectangle(C64Brushes.Items[Random.Next(C64Brushes.Count)],
                                       0, y, FullScreen.Width, rasterWidth);
                y += rasterWidth;
            }
            while (y < FullScreen.Height);
            FullGraphics.ResetClip();

            //else if (back)
            //{
            //    int y = ScreenTop;
            //    do
            //    {
            //        Brush b = C64Brushes.Items[random.Next(C64Brushes.Count)];
            //        graphics.FillRectangle(b, ScreenLeft, y, ScreenLeft + ScreenWidth, rasterWidth);
            //        y += rasterWidth;
            //    }
            //    while (y < ScreenTop + ScreenHeight);
            //}
            //else if (frame)
            //{
            //    graphics.SetClip(backRect, CombineMode.Exclude);
            //    int y = ScreenTop;
            //    do
            //    {
            //        Brush b = C64Brushes.Items[random.Next(C64Brushes.Count)];
            //        graphics.FillRectangle(b, ScreenLeft, y, ScreenLeft + ScreenWidth, rasterWidth);
            //        y += rasterWidth;
            //    }
            //    while (y < ScreenTop + ScreenHeight);

            //    graphics.ResetClip();
            //}
        }
    }
}