﻿using System.Drawing;

namespace Laserbrain.C64Screensaver
{
    internal class C64Character
    {
        private readonly bool[,] charMap;

        public C64Character(bool[,] charMap)
        {
            this.charMap = charMap;
        }

        public const int Width = 8;
        public const int Height = 8;

        public void Draw(int x, int y, C64Palette? foreColor, C64Palette? backColor, C64Screen screen)
        {
            x = x * Width + screen.ScreenLeft;
            y = y * Height + screen.ScreenTop;
            if (foreColor.HasValue && backColor.HasValue)
            {
                Color fc = C64Colors.Items[(int)foreColor.Value];
                Color bc = C64Colors.Items[(int)backColor.Value];
                for (int cy = 0; cy < Height; cy++)
                    for (int cx = 0; cx < Width; cx++)
                        screen.FullScreen.SetPixel(x + cx, y + cy, charMap[cx, cy] ? fc : bc);
            }
            else
            {
                Color? fc = foreColor.HasValue ? C64Colors.Items[(int)foreColor.Value] : (Color?)null;
                Color? bc = backColor.HasValue ? C64Colors.Items[(int)backColor.Value] : (Color?)null;
                for (int cy = 0; cy < Height; cy++)
                {
                    for (int cx = 0; cx < Width; cx++)
                    {
                        Color c;
                        if (charMap[cx, cy])
                            c = fc.HasValue ? fc.Value : screen.OriginalScreen.GetPixel(x + cx, y + cy);
                        else
                            c = bc.HasValue ? bc.Value : screen.OriginalScreen.GetPixel(x + cx, y + cy);
                        screen.FullScreen.SetPixel(x + cx, y + cy, c);
                    }
                }
            }
        }

        public void DrawBig(int x, int y, C64Palette? foreColor, C64Palette? backColor, C64Screen screen)
        {
            x = x * Width + screen.ScreenLeft;
            y = y * Height + screen.ScreenTop;
            if (foreColor.HasValue && backColor.HasValue)
            {
                Brush fb = C64Brushes.Items[(int)foreColor.Value];
                Brush bb = C64Brushes.Items[(int)backColor.Value];
                for (int cy = 0; cy < Height; cy++)
                {
                    for (int cx = 0; cx < Width; cx++)
                    {
                        screen.FullGraphics.FillRectangle(charMap[cx, cy] ? fb : bb,
                            x + cx * Width, y + cy * Height, Width, Height);
                    }
                }
            }
            else
            {
                Brush fb = foreColor.HasValue ? C64Brushes.Items[(int)foreColor.Value] : null;
                Brush bb = backColor.HasValue ? C64Brushes.Items[(int)backColor.Value] : null;
                for (int cy = 0; cy < Height; cy++)
                {
                    for (int cx = 0; cx < Width; cx++)
                    {
                        int xx = x + cx * Width;
                        int yy = y + cy * Height;
                        Brush b = charMap[cx, cy] ? fb : bb;
                        if (b != null)
                            screen.FullGraphics.FillRectangle(b, xx, yy, Width, Height);
                        else
                            screen.FullGraphics.DrawImage(screen.OriginalScreen, xx, yy, new Rectangle(xx, yy, Width, Height), GraphicsUnit.Pixel);
                    }
                }
            }
        }
    }
}