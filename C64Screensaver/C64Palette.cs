﻿using System;
using System.Drawing;
using System.Linq;

namespace Laserbrain.C64Screensaver
{
    public enum C64Palette
    {
        Black, White, Red, Cyan, Purple, Green, Blue, Yellow,
        Orange, Brown, LightRed, DarkGrey, Grey, LightGreen, LightBlue, LightGrey,
    }

    public struct C64Colors
    {
        private static readonly Tuple<C64Palette, C64Palette>[] BadCombinationDict = new[]
        {
            new Tuple<C64Palette, C64Palette>(C64Palette.Black, C64Palette.Blue),
            new Tuple<C64Palette, C64Palette>(C64Palette.White, C64Palette.Yellow),
            new Tuple<C64Palette, C64Palette>(C64Palette.Red, C64Palette.Brown),
            new Tuple<C64Palette, C64Palette>(C64Palette.Red, C64Palette.DarkGrey),
            new Tuple<C64Palette, C64Palette>(C64Palette.Cyan, C64Palette.Yellow),
            new Tuple<C64Palette, C64Palette>(C64Palette.Cyan, C64Palette.LightGreen),
            new Tuple<C64Palette, C64Palette>(C64Palette.Purple, C64Palette.Orange),
            new Tuple<C64Palette, C64Palette>(C64Palette.Purple, C64Palette.LightBlue),
            new Tuple<C64Palette, C64Palette>(C64Palette.Green, C64Palette.LightRed),
            new Tuple<C64Palette, C64Palette>(C64Palette.Green, C64Palette.Grey),
            new Tuple<C64Palette, C64Palette>(C64Palette.Green, C64Palette.LightGrey),
            new Tuple<C64Palette, C64Palette>(C64Palette.Yellow, C64Palette.LightGreen),
            new Tuple<C64Palette, C64Palette>(C64Palette.Orange, C64Palette.LightBlue),
            new Tuple<C64Palette, C64Palette>(C64Palette.Brown, C64Palette.DarkGrey),
            new Tuple<C64Palette, C64Palette>(C64Palette.LightRed, C64Palette.Grey),
            new Tuple<C64Palette, C64Palette>(C64Palette.Red, C64Palette.LightBlue),
            new Tuple<C64Palette, C64Palette>(C64Palette.Cyan, C64Palette.LightGrey),
            new Tuple<C64Palette, C64Palette>(C64Palette.Purple, C64Palette.Grey),
            new Tuple<C64Palette, C64Palette>(C64Palette.Yellow, C64Palette.LightGrey),
            new Tuple<C64Palette, C64Palette>(C64Palette.Orange, C64Palette.LightRed),
            new Tuple<C64Palette, C64Palette>(C64Palette.Orange, C64Palette.Grey),
            new Tuple<C64Palette, C64Palette>(C64Palette.LightRed, C64Palette.LightGreen),
            new Tuple<C64Palette, C64Palette>(C64Palette.LightRed, C64Palette.LightBlue),
            new Tuple<C64Palette, C64Palette>(C64Palette.LightGreen, C64Palette.LightGrey),
        };

        public static readonly Color Black = Color.FromArgb(0, 0, 0);
        public static readonly Color White = Color.FromArgb(255, 255, 255);
        public static readonly Color Red = Color.FromArgb(197, 65, 84);
        public static readonly Color Cyan = Color.FromArgb(147, 252, 229);
        public static readonly Color Purple = Color.FromArgb(232, 80, 242);
        public static readonly Color Green = Color.FromArgb(78, 225, 78);
        public static readonly Color Blue = Color.FromArgb(58, 70, 228);
        public static readonly Color Yellow = Color.FromArgb(253, 239, 118);
        public static readonly Color Orange = Color.FromArgb(229, 131, 64);
        public static readonly Color Brown = Color.FromArgb(163, 103, 9);
        public static readonly Color LightRed = Color.FromArgb(255, 139, 147);
        public static readonly Color DarkGrey = Color.FromArgb(123, 125, 121);
        public static readonly Color Grey = Color.FromArgb(175, 180, 174);
        public static readonly Color LightGreen = Color.FromArgb(165, 255, 151);
        public static readonly Color LightBlue = Color.FromArgb(133, 153, 255);
        public static readonly Color LightGrey = Color.FromArgb(207, 209, 205);

        public const int Count = 16;

        public static readonly Color[] Items = new[]
        {
            Black, White, Red, Cyan, Purple, Green, Blue, Yellow,
            Orange, Brown, LightRed, DarkGrey, Grey, LightGreen, LightBlue, LightGrey,
        };

        public static bool BadCombination(C64Palette? color1, C64Palette? color2)
        {
            if (!color1.HasValue || !color2.HasValue)
                return false;
            return (color1 == color2) || BadCombinationDict.Any(tuple => ((tuple.Item1 == color1) && (tuple.Item2 == color2)) || ((tuple.Item1 == color2) && (tuple.Item2 == color1)));
        }
    }

    public struct C64Pens
    {
        public static readonly Pen Black = new Pen(C64Colors.Black);
        public static readonly Pen White = new Pen(C64Colors.White);
        public static readonly Pen Red = new Pen(C64Colors.Red);
        public static readonly Pen Cyan = new Pen(C64Colors.Cyan);
        public static readonly Pen Purple = new Pen(C64Colors.Purple);
        public static readonly Pen Green = new Pen(C64Colors.Green);
        public static readonly Pen Blue = new Pen(C64Colors.Blue);
        public static readonly Pen Yellow = new Pen(C64Colors.Yellow);
        public static readonly Pen Orange = new Pen(C64Colors.Orange);
        public static readonly Pen Brown = new Pen(C64Colors.Brown);
        public static readonly Pen LightRed = new Pen(C64Colors.LightRed);
        public static readonly Pen DarkGrey = new Pen(C64Colors.DarkGrey);
        public static readonly Pen Grey = new Pen(C64Colors.Grey);
        public static readonly Pen LightGreen = new Pen(C64Colors.LightGreen);
        public static readonly Pen LightBlue = new Pen(C64Colors.LightBlue);
        public static readonly Pen LightGrey = new Pen(C64Colors.LightGrey);

        public const int Count = 16;

        public static readonly Pen[] Items = new[]
        {
            Black, White, Red, Cyan, Purple, Green, Blue, Yellow,
            Orange, Brown, LightRed, DarkGrey, Grey, LightGreen, LightBlue, LightGrey,
        };
    }

    public struct C64Brushes
    {
        public static readonly Brush Black = new SolidBrush(C64Colors.Black);
        public static readonly Brush White = new SolidBrush(C64Colors.White);
        public static readonly Brush Red = new SolidBrush(C64Colors.Red);
        public static readonly Brush Cyan = new SolidBrush(C64Colors.Cyan);
        public static readonly Brush Purple = new SolidBrush(C64Colors.Purple);
        public static readonly Brush Green = new SolidBrush(C64Colors.Green);
        public static readonly Brush Blue = new SolidBrush(C64Colors.Blue);
        public static readonly Brush Yellow = new SolidBrush(C64Colors.Yellow);
        public static readonly Brush Orange = new SolidBrush(C64Colors.Orange);
        public static readonly Brush Brown = new SolidBrush(C64Colors.Brown);
        public static readonly Brush LightRed = new SolidBrush(C64Colors.LightRed);
        public static readonly Brush DarkGrey = new SolidBrush(C64Colors.DarkGrey);
        public static readonly Brush Grey = new SolidBrush(C64Colors.Grey);
        public static readonly Brush LightGreen = new SolidBrush(C64Colors.LightGreen);
        public static readonly Brush LightBlue = new SolidBrush(C64Colors.LightBlue);
        public static readonly Brush LightGrey = new SolidBrush(C64Colors.LightGrey);

        public const int Count = 16;

        public static readonly Brush[] Items = new[]
        {
            Black, White, Red, Cyan, Purple, Green, Blue, Yellow,
            Orange, Brown, LightRed, DarkGrey, Grey, LightGreen, LightBlue, LightGrey,
        };
    }
}