﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;

namespace Laserbrain.C64Screensaver
{
    internal class C64Sprite : IDisposable
    {
        private readonly C64Screen screen;
        private readonly Bitmap spriteBitmap;
        private readonly bool[,] spriteMap = new bool[Width, Height];
        private C64Palette color;
        private readonly Graphics fullScreenGraphics;

        public C64Sprite(string resourceName, C64Screen screen)
        {
            this.screen = screen;

            spriteBitmap = new Bitmap(24, 21, PixelFormat.Format24bppRgb);
            spriteBitmap.SetResolution(screen.FullScreen.HorizontalResolution,
                screen.FullScreen.VerticalResolution);
            spriteBitmap.MakeTransparent(System.Drawing.Color.Empty);

            fullScreenGraphics = Graphics.FromImage(screen.FullScreen);

            Assembly assembly = Assembly.GetExecutingAssembly();
            using (Stream imageStream = assembly.GetManifestResourceStream(resourceName))
            {
                if (imageStream != null)
                {
                    using (Bitmap originalBitmap = new Bitmap(imageStream))
                    {
                        if ((originalBitmap.Width != 24) && (originalBitmap.Height != 21))
                            throw new ArgumentException("Bad sprite image size, should be 24x21");
                        for (int y = 0; y < 21; y++)
                            for (int x = 0; x < 24; x++)
                                spriteMap[x, y] = (originalBitmap.GetPixel(x, y).R < 200);
                    }
                }
            }
            Color = C64Palette.Black;
        }

        public void Dispose()
        {
            spriteBitmap.Dispose();
            fullScreenGraphics.Dispose();
        }

        public int X { get; set; }

        public int Y { get; set; }

        public const int Width = 24;
        public const int Height = 21;

        public C64Palette Color
        {
            get { return color; }
            set
            {
                color = value;
                Color actualColor = C64Colors.Items[(int)color];
                for (int y = 0; y < Height; y++)
                    for (int x = 0; x < Width; x++)
                        if (spriteMap[x, y])
                            spriteBitmap.SetPixel(x, y, actualColor);
            }
        }

        public void Draw()
        {
            fullScreenGraphics.DrawImage(spriteBitmap,
                X + screen.ScreenLeft - 32,
                Y + screen.ScreenTop - 36);
        }
    }
}