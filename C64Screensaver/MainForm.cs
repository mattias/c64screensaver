﻿using System;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using CPI.Plot3D;

namespace Laserbrain.C64Screensaver
{
    public partial class MainForm : Form
    {
        #region Preview API's

        [DllImport("user32.dll")]
        static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);

        [DllImport("user32.dll")]
        static extern int SetWindowLong(IntPtr hWnd, int nIndex, IntPtr dwNewLong);

        [DllImport("user32.dll", SetLastError = true)]
        static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll")]
        static extern bool GetClientRect(IntPtr hWnd, out Rectangle lpRect);

        #endregion Preview API's

        private enum AnimKind
        {
            Letters,
            Balloons,
            Box,
            BigTime,
        }

        private const int AnimSwitchDefaultCount = 63;
        private const int ColorSwitchDefaultCount = 47;

        private Point? originalLocation;
        private static readonly Random Random = new Random();
        private bool cursorVisible;
        private int cursorBlinkCounter;
        private int colorSwitchCountDown = ColorSwitchDefaultCount;

        private readonly C64Screen c64Screen;
        private C64Font c64Font;
        private C64Sprite balloon1;
        private C64Sprite balloon2;
        private C64Sprite[] letterSprites;
        private const int HalfCubeSize = 50;

        private C64Palette? foreColor = C64Palette.Blue;
        private C64Palette? backColor = C64Palette.LightBlue;

        private double counter = Random.Next(1000);
        private AnimKind animKind;
        private int animSwitchCountDown = AnimSwitchDefaultCount;

        public MainForm(Size size)
        {
            InitializeComponent();

            SetBounds(0, 0, size.Width, size.Height);
            c64Screen = new C64Screen(Size.Width, Size.Height, false, Size);

            InitFont();
            InitSprites();
            UpdateScreen();

            Cursor.Hide();

            TopMost = true;
            KeyDown += MainForm_KeyDown;
            Click += MainForm_Click;
            MouseMove += MainForm_MouseMove;
        }

        public MainForm(IntPtr previewHandle)
        {
            try
            {
                InitializeComponent();

                SetParent(Handle, previewHandle);

                // Make this a child window, so when the select
                // screensaver dialog closes, this will also close
                SetWindowLong(Handle, -16, new IntPtr(GetWindowLong(Handle, -16) | 0x40000000));

                // Set our window's size to the size of our window's new parent
                Rectangle parentRect;
                GetClientRect(previewHandle, out parentRect);

                SetBounds(0, 0, parentRect.Size.Width, parentRect.Size.Height);
                c64Screen = new C64Screen(Size.Width, Size.Height, true, Screen.PrimaryScreen.Bounds.Size);

                InitFont();
                InitSprites();
                UpdateScreen();
            }
            catch (Exception)
            {
                Environment.Exit(0);
            }
        }

        private void InitFont()
        {
            c64Font = new C64Font("Laserbrain.C64Screensaver.C64Font.png", c64Screen);
        }

        private void InitSprites()
        {
            const string resourcePrefix = "Laserbrain.C64Screensaver.Sprites.";
            balloon1 = new C64Sprite(resourcePrefix + "BalloonSprite.png", c64Screen);
            balloon1.X = Random.Next(32, 329);
            balloon1.Y = Random.Next(36, 216);
            balloon1.Color = C64Palette.Yellow;

            balloon2 = new C64Sprite(resourcePrefix + "BalloonSprite.png", c64Screen);
            balloon2.X = Random.Next(32, 329);
            balloon2.Y = Random.Next(36, 216);
            balloon2.Color = C64Palette.Cyan;

            letterSprites = new C64Sprite[9];
            letterSprites[0] = new C64Sprite(resourcePrefix + "SpriteLetterD.png", c64Screen);
            letterSprites[1] = new C64Sprite(resourcePrefix + "SpriteLetterI.png", c64Screen);
            letterSprites[2] = new C64Sprite(resourcePrefix + "SpriteLetterV.png", c64Screen);
            letterSprites[3] = new C64Sprite(resourcePrefix + "SpriteLetterE.png", c64Screen);
            letterSprites[4] = new C64Sprite(resourcePrefix + "SpriteLetterR.png", c64Screen);
            letterSprites[5] = new C64Sprite(resourcePrefix + "SpriteLetterS.png", c64Screen);
            letterSprites[6] = new C64Sprite(resourcePrefix + "SpriteLetterI.png", c64Screen);
            letterSprites[7] = new C64Sprite(resourcePrefix + "SpriteLetterF.png", c64Screen);
            letterSprites[8] = new C64Sprite(resourcePrefix + "SpriteLetterY.png", c64Screen);

            foreach (var letterSprite in letterSprites)
                letterSprite.Color = C64Palette.Blue;
        }

        private void MainForm_KeyDown(object sender, KeyEventArgs e)
        {
            Application.Exit();
        }

        private void MainForm_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void MainForm_MouseMove(object sender, MouseEventArgs e)
        {
            if (!originalLocation.HasValue)
                originalLocation = e.Location;
            else if (Math.Abs(e.X - originalLocation.Value.X) > 20 || Math.Abs(e.Y - originalLocation.Value.Y) > 20)
                Application.Exit();
        }

        private void MainForm_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                c64Screen.Draw(e.Graphics);
            }
            catch { }
        }

        private void updateTimer_Tick(object sender, EventArgs e)
        {
            UpdateScreen();
        }

        private void UpdateScreen()
        {
            try
            {
                cursorBlinkCounter--;
                if (cursorBlinkCounter <= 0)
                {
                    cursorVisible = !cursorVisible;
                    cursorBlinkCounter = 8;
                }

                colorSwitchCountDown--;
                if (colorSwitchCountDown <= 0)
                {
                    colorSwitchCountDown = ColorSwitchDefaultCount;
                    SwitchColors();
                }

                animSwitchCountDown--;
                if (animSwitchCountDown <= 0)
                {
                    animSwitchCountDown = AnimSwitchDefaultCount;
                    SwitchAnim();
                }

                counter += 0.5;

                c64Screen.Clear(backColor);
                c64Font.DrawString(0, 1, foreColor, backColor, "    **** COMMODORE 64 BASIC V2 ****");
                c64Font.DrawString(0, 3, foreColor, backColor, " 64K RAM SYSTEM  38911 BASIC BYTES FREE");
                c64Font.DrawString(0, 5, foreColor, backColor, "READY.");
                if (cursorVisible)
                    c64Font.DrawString(0, 6, backColor, foreColor, " ");
                c64Font.DrawString(10, 24, foreColor, backColor, DateTime.Now.ToString("yyyy'-'MM'-'dd'  'HH':'mm':'ss"));

                switch (animKind)
                {
                    case AnimKind.Letters:
                        DrawSpriteLetters();
                        break;
                    case AnimKind.Balloons:
                        DrawBalloons();
                        break;
                    case AnimKind.Box:
                        DrawBox();
                        break;
                    case AnimKind.BigTime:
                        DrawBigTime();
                        break;
                }

                if (((counter / 10) % 7) < 3)
                    c64Screen.DrawRaster(false, true, 2);
                else
                    c64Screen.DrawFrame(foreColor);

                Invalidate();
            }
            catch { }
        }

        private void DrawSpriteLetters()
        {
            int x = (C64Screen.ScreenWidth - (letterSprites.Length * C64Sprite.Width)) / 2 + 32;
            const int y = 100;

            double c = counter * 2;
            foreach (var letterSprite in letterSprites)
            {
                letterSprite.X = (int)(Math.Cos(c / 9) * Math.Sin(c / 7) * 30 + x);
                letterSprite.Y = (int)(Math.Sin(c / 5) * Math.Sin(c / 20) * 50 + 36 + y);
                letterSprite.Draw();
                x += C64Sprite.Width;
                c += 2;
            }
        }

        private void DrawBalloons()
        {
            balloon1.X = (int)(Math.Sin(counter / 17) * Math.Cos(counter / 18) * 130 + 180);
            balloon1.Y = (int)(Math.Cos(counter / 9) * Math.Cos(counter / 66) * 80 + 126);
            balloon1.Draw();
            balloon2.X = (int)(Math.Cos(counter / 50) * Math.Sin(counter / 50) * 130 + 180);
            balloon2.Y = (int)(Math.Sin(counter / 5) * Math.Sin(counter / 20) * 80 + 126);
            balloon2.Draw();
        }

        private void DrawBox()
        {
            using (Plotter3D p3D = new Plotter3D(c64Screen.FullGraphics,
                new Point3D(c64Screen.ScreenLeft + 160, c64Screen.ScreenTop + 120 - HalfCubeSize, -200)))
            {
                p3D.Location = new Point3D(
                    (float)(Math.Sin(counter / 23) * Math.Cos(counter / 11) * 300 + 140),
                    (float)(Math.Cos(counter / 9) * Math.Cos(counter / 66) * 120 + 150),
                    (float)(Math.Sin(counter / 20) * Math.Sin(counter / 6) * 50 + 150));
                p3D.PenColor = C64Colors.Items[(int)balloon1.Color];
                p3D.PenUp();
                p3D.Forward(HalfCubeSize);
                p3D.TurnRight(90);
                p3D.Forward(HalfCubeSize);
                p3D.TurnDown(90);
                p3D.Forward(HalfCubeSize);
                p3D.TurnUp(90);
                p3D.TurnRight(counter * 10);
                p3D.TurnDown(counter * 15);
                p3D.TurnDown(90);
                p3D.TurnRight(180);
                p3D.Forward(HalfCubeSize);
                p3D.TurnDown(90);
                p3D.Forward(HalfCubeSize);
                p3D.TurnLeft(90);
                p3D.Forward(HalfCubeSize);
                p3D.TurnLeft(180);
                p3D.PenDown();
                const float cubeSize = HalfCubeSize * 2;
                for (int side = 0; side < 4; side++)
                {
                    for (int line = 0; line < 4; line++)
                    {
                        p3D.Forward(cubeSize);  // Draw a line sideLength long
                        p3D.TurnRight(90);      // Turn right 90 degrees
                    }

                    p3D.Forward(cubeSize);
                    p3D.TurnDown(90);
                }
            }
        }

        private void DrawBigTime()
        {
            DateTime now = DateTime.Now;
            string time = now.ToString((now.Millisecond < 500) ? "HH':'mm" : "HH' 'mm");
            int x = 0;
            foreach (var c64Char in time.Select(c => c64Font.GetChar(c)))
            {
                c64Char.DrawBig(x, 9, foreColor, backColor, c64Screen);
                x += 8;
            }
        }

        private void SwitchAnim()
        {
            switch (animKind)
            {
                case AnimKind.Letters: animKind = AnimKind.Balloons; break;
                case AnimKind.Balloons: animKind = AnimKind.Box; break;
                case AnimKind.Box: animKind = AnimKind.BigTime; break;
                case AnimKind.BigTime: animKind = AnimKind.Letters; break;
            }
        }

        private C64Palette? GetRandomColor()
        {
            int paletteIndex = Random.Next(C64Colors.Count + 1);
            if (paletteIndex == C64Colors.Count)
                return null;
            return (C64Palette)paletteIndex;
        }

        private void SwitchColors()
        {
            foreColor = GetRandomColor();

            do
            {
                backColor = GetRandomColor();
            }
            while (C64Colors.BadCombination(backColor, foreColor));

            do
            {
                balloon1.Color = (C64Palette)Random.Next(C64Colors.Count);
            }
            while (C64Colors.BadCombination(balloon1.Color, backColor));
            do
            {
                balloon2.Color = (C64Palette)Random.Next(C64Colors.Count);
            }
            while (C64Colors.BadCombination(balloon2.Color, backColor) ||
                   C64Colors.BadCombination(balloon2.Color, balloon1.Color));

            C64Palette letterColor;
            do
            {
                letterColor = (C64Palette)Random.Next(C64Colors.Count);
            }
            while (C64Colors.BadCombination(letterColor, backColor) ||
                   C64Colors.BadCombination(letterColor, balloon1.Color) ||
                   C64Colors.BadCombination(letterColor, balloon2.Color));
            foreach (var letterSprite in letterSprites)
                letterSprite.Color = letterColor;
        }
    }
}